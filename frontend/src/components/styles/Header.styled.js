import styled from "styled-components";

export const NavContainer = styled.nav`
  width: 100%;
  height: 56px;
  color: #ffff;
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 15px 15px;
  background-color: #161d2f;
`;

export const Div = styled.div``;

export const IconsDiv = styled.div`
  width: 45%;
  display: flex;
  justify-content: space-between;
`;

export const UserDiv = styled.div`
  border: 1px solid white;
  width: 24px;
  height: 24px;
  border-radius: 50%;
  display: flex;
  justify-content: space-between;
`;

// navbar icons styles

/* Movie icon styling */

import styled from "styled-components";

import { RecomendedSection, RecomendedContainer } from "./Recomended.styled";

/* Recomended Section and Container */
export const CardsSection = styled(RecomendedSection)``;

export const CardsContainer = styled(RecomendedContainer)``;

/* Cards styling */
